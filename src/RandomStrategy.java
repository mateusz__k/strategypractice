import java.util.Random;

public class RandomStrategy implements IInputStrategy {
    private Random random =new Random();

    public RandomStrategy() {
        this.random = new Random();
    }

    @Override
    public String getString() {
        StringBuilder sb = new StringBuilder();
        int length = random.nextInt(100);
        for (int i = 0; i < length; i++) {
            // generating random character with ascii from 97 to ...
            sb.append((char) (random.nextInt(32) + 97));
        }

        return sb.toString();
    }

    @Override
    public int getInt() {
        return random.nextInt();
    }

    @Override
    public double getDouble() {
        return random.nextDouble();
    }
}

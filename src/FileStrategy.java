import java.io.*;
import java.util.Scanner;

public class FileStrategy implements IInputStrategy {


    @Override
    public int getInt() {
        try {
            Scanner scanner = new Scanner(new File("file3.txt"));
            return scanner.nextInt();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public String getString() {
        try {
            Scanner scanner = new Scanner(new File("file.txt"));
            return scanner.next();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public double getDouble() {
        try {
            Scanner scanner = new Scanner(new File("file2.txt"));
            return scanner.nextDouble();
        } catch (FileNotFoundException e) {
            e.printStackTrace();

            return 0;
        }
    }
}

